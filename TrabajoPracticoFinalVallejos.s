.data
/* Definicion de datos */
/* ___________________________________________________|\n fila0	| largo de fila = 52
                                                      |\n   1	|
     *** EL JUEGO DEL AHORCADO - ORGA 1 ***           |\n   2	|
   ___________________________________________________|\n   3	|
                                                      |\n   4	|
                                                      |\n   5	|
             +------------+                           |\n   6	|
             |            | [23,7] coordenada         |\n   7	|
             |            o                           |\n   8	|
             |           /|\                          |\n   9	|
             |            |                           |\n   10	|
             |           / \                          |\n   11	|
             |                                        |\n   12	|
             |                                        |\n   13	|
             |                                        |\n   14	|
    +-------------------------------------------+     |\n   15	|
    |                                           |     |\n   16	|
    |    _ _ _ _ _ _ _ _ _ _ _ _ _ _            |     |\n   17	|
    |                                           |     |\n   18	|
    +-------------------------------------------+     |\n   19	|
*/







//---------- Mapa ---------
mapa: .asciz "___________________________________________________|\n                                                   |\n     *** EL JUEGO DEL AHORCADO - ORGA 1 ***        |\n___________________________________________________|\n                                                   |\n                                                   |\n          +------------+                           |\n          |            |                           |\n          |                                        |\n          |                                        |\n          |                                        |\n          |                                        |\n          |                                        |\n          |                                        |\n          |                                        |\n +-------------------------------------------+     |\n |                                           |     |\n |                                           |     |\n |                                           |     |\n +-------------------------------------------+     |\n"
enter: .ascii "\n"
cls: .asciz "\x1b[H\x1b[2J" //una manera de borrar la pantalla usando ansi escape codes
lencls = .-cls
longitud = . - mapa // longitud del mapa


//preguntaAproximada: .asciz "¿En que año se creo la bandera Argentina:?:\n"


//respuestaCorrecta: .asciz "Respuesta correcta se le suman 2 vidas\n"

//mensajeRespuestaIncorrecta: .asciz "Respuesta incorrecta\n"



//respuesta1_intervalo1: .word 0x6D6
//respuesta1_intervalo2: .word 0x73A



//-- Imput del usuario

inputUsuario: .asciz "   " // FIX cambiado de .ascii "  " a .asciz "  " para poder solucionar el input de bandera


//------------ Palabras

palabra1: .asciz "bandera"
palabraMisteriosa1: .asciz "@@@@@@@" 
palabra2: .asciz "caballo" // longitud de 8

//----------- Constantes direccionales a filas del mapa

fila8: .word 0x1a8 // 424
fila9: .word 0x1dd // 477
fila10: .word 0x212  //530
fila11: .word 0x247 //583

//----------- Dibujos -----------------

cabeza: .asciz "O "
torso: .asciz "| "
brazoIzq: .asciz "/ "
brazoDer: .word 0x5c //.asciz "\ " // su escritura en el ahorcado no funciona
cadera: .asciz "| "
pieIzq: .asciz "/ "
pieDer: .word 0x5c //.asciz "\ "  // Su escritura en el ahoracado no funciona

//------------- Mensajes al usuario -------

ingreseLetra: .asciz "Ingrese una letra:\n"
largoIngreseLetra: .word 0x13

agregadasCorrectamente1: .asciz "7 letras fueron agregadas correctamente"
largoMensaje: .word 0x27

numeroIntento: .asciz "0"

mensajePerdiste: .asciz "Te quedaste sin vidas. Perdiste el juego\n"

mensajeGanaste: .asciz "Ganaste el juego.¡Felicidades!\n"

ingresarX: .asciz "Ingrese la coordenada en X:\n"
ingresarY: .asciz "Ingrese la coordenada en Y:\n"


saltoDeLinea: .asciz "\n"

//-----------------------

mascara: .word 0x00000001

//----------------------------------------------------------
.text @ Defincion de codigo del programa
//----------------------------------------------------------

imprimirPalabraSinLimpiarPantalla: .fnstart
      //Parametros inputs: r1=puntero al string que queremos imprimir r2=longitud de lo que queremos imprimir
	push {r7}
	push {r0}
	mov r7, #4 // Salida por pantalla
	mov r0, #1 // Indicamos a SWI que sera una cadena
	swi 0 // SWI, Software interrup
	pop {r0}
	pop {r7}
	bx lr //salimos de la funcion mifuncion
	.fnend



//----------------------------------------------------------
imprimirString: .fnstart
      //Parametros inputs: r1=puntero al string que queremos imprimir r2=longitud de lo que queremos imprimir
	push {lr}
	push {r1}
	push {r2}
	bl clearScreen
	pop {r2}
	pop {r1}
	mov r7, #4 // Salida por pantalla
	mov r0, #1 // Indicamos a SWI que sera una cadena
	swi 0 // SWI, Software interrup
	pop {lr}
	bx lr //salimos de la funcion mifuncion
	.fnend
//----------------------------------------------------------
clearScreen: .fnstart
	mov r0, #1
	ldr r1, =cls
	ldr r2, =lencls
	mov r7, #4
	swi #0
	bx lr //salimos de la funcion mifuncion
	.fnend
//----------------------------------------------------------
pedirLetra: // pido una letra al usuario y guardo su valor en r1
	.fnstart
	//---------- Pido input
	push {r7}
	push {r0}
	push {r2}
	mov r7,#3  //indicamos lectura por teclado
	mov r0,#0  // indicamos que el ingreso es una cadena
	mov r2,#2 // leer el 1er caracter
        ldr r1,=inputUsuario   // Interrupcion para esperar la letra del usuario y obtenerla en el r1
	swi 0
	pop {r2}
	pop {r0}
	pop {r7}
	bx lr
	.fnend

//----------------------------------------------------------
pedirNumeroX:
	.fnstart
	mov r7,#3 // Leer teclado
	mov r0,#0 // input por teclado
	mov r2,#4 // leer 2 caracteres
	ldr r1,=inputUsuario
	swi 0
	bx lr
	.fnend

//----------------------------------------------------------
pedirNumeroY:
	.fnstart
	mov r7,#3 // Leer teclado
	mov r0,#0 // input por teclado
	mov r2,#4 // leer 1 caracteres
	ldr r1,=inputUsuario
	swi 0
	bx lr
	.fnend


.global main 	// global, visible en todo el programa
	main:  // main solo se va a encargar de inicializar ciertos datos al principio del juego.
	ldr r1,=mascara // mascara para tomar el bit menos significativo
	and r0,r0,r1  // hago el and para que me quede solo el bit menos significativo
	mov r1,#1 // inicializo r1 a 1 
	cmp r0,r1 // comparo si el resultado del and es 1
	beq punteroPalabra2 // cambio a la 2da palabra a adivinar
AcaLLameAPalabra2:
	mov r11,#7 // Este es mi contador De arrobas. Utilizado para saber si el usuario adivino todas las letras. Si el contador es 0. Entonces no hay mas arrobas y por lo tanto no hay letras a adivinar. En ese caso, gano el juego
	mov r5,#7 // Inicio las vidas en 7
	mov r10,#0 //-- r10 será mi contador de posicion. Utilizado para reemplazar los @ arrobas de la palabra misteriosa y formar la palabra original. Puedo hacer un push antes de llamar a la funcion

punteroPalabra2:
	ldr r0,=palabra2 // puntero a caballo
	ldr r0,[r0] // tomo la palabra caballo
	ldr r1,=palabra1 // puntero a palabra1
	str r0,[r1] // ahora palabra1 = caballo
	b AcaLLameAPalabra2


cicloParaPedirLetra:
	cmp r11,#0 // r11 Es mi contador de arrobas. Si tengo 0 arrobas acerte todas las letras. Por lo que gane el juego
	beq ganaste // si r11 == 0 ganaste el juego
//------- Limpio la Pantalla
	ldr r1,=saltoDeLinea
	mov r2,#2
	bl imprimirString
//----------- Imprimo el mapa
	ldr r1,=mapa
	ldr r2,=longitud
	bl imprimirPalabraSinLimpiarPantalla
//-------------- Comparo si sus vidas son 0 para seguir jugando o no
	cmp r5,#0 // Pido Letras hasta que vidas sea 0
	beq punteria // Parte 3 del TP
//	beq perdiste // Si las vidas son 0. Pierdo el juego
//--------------------------------------------------------
//--------- FIX Imprimo PalabraMisteriosa sin limpiar la pantalla------
	ldr r1,=palabraMisteriosa1
	mov r2,#7
	bl imprimirPalabraSinLimpiarPantalla
//----------------------------------------------------------
// ------------ Imprimo un salto de linea -----------
	ldr r1,=saltoDeLinea
	mov r2,#2
	bl imprimirPalabraSinLimpiarPantalla
//-------------------------------
//---------- Imprimo " IngreseLetra" al usuario ---- //
	ldr r1,=ingreseLetra // puntero al mensaje ingreseLetra
	mov r2,#20 // longitud de la cadena ingreseLetra
	bl imprimirPalabraSinLimpiarPantalla // Imprimo el mensaje "Ingrese una letra:" con los parametros anteriores de r1 y r2
//--------------------------------------------------
	bl pedirLetra // Funcion pedirLetra. Espera que el usuario ingrese una letra y la guarda el puntero en r1. r1 es  parametro para cicloVerificacion
	ldr r2,=palabra1 // Puntero a mi palabra original. Parametro para cicloVerificacion
	mov r9,#0 // inicio r9 en 0 para prepararme para el otro cicloVerificacion
	b cicloVerificacion// Verifico la letra del usuario con la primer letra de palabra
AcaLLameACicloVerificacion:
	mov r8,#0 // Igualo a 0 mi contador de posiciones Para prepararme para el proximo ciclo. Si no lo igualo a 0. Mi contador de posiciones no empiezaria en la posicion 0
	cmp r9,#0 // r9 Me avisa si el usuario acerto o no la letra. If == 0 : No acerto.| else acerto
	beq restarVida // Si al terminar el recorrido minguna letra es igual  simple resta de r5 - 1
	b cicloParaPedirLetra // Repito el mismo cicloParaPedirLetra

cicloVerificacion: //--- Dado el input del usuario y la palabra a adivinar. Esta funcion Verifica la letra del usuario con todas las letras de la palabra a adivinar.
		// Si letra de palabra == letra del usuario. Mi contador de @ arrobas se resta en 1. en caso contrario. Resto una vida y continuo pidiendo letras
	ldrb r1,[r1] // Obtengo la letra del usuario en r1
	mov r3,#0 // FIX -- Inicio r3 a 0 para evitar errores
	ldrb r3,[r2] // FIX Obtengo el valor de la posicion de r2 . La letra en sí
	cmp r3,#0 // Comparo que sea el final de la cadena
	beq AcaLLameACicloVerificacion // Si es null. Vuelvo a donde llamaron a esta funcion. Salgo de la funcion
	cmp r1,r3 // Comparo la letra del usuario (r1) con la la letra de la palabra (r3)
	beq verificacionCorrecta //-- En caso de ser la misma. La verificacion es correcta y debo reemplazar la palabra misteriosa por la letra dada
AcaLLameAVerificacionCorrecta:
	add r2,r2,#1 // Paso a la siguiente letra
	add r8,r8,#1 // sumo 1 A mi contador de posiciones
	ldr r1,=inputUsuario //FIX: Vuelvo al puntero del input del usuario. Ya que al ciclar da error
	b cicloVerificacion // Sigo con la funcion hasta que sea el final de la palabra


verificacionCorrecta:
	add r9,#1  // Aviso a r9 que adivino 1 letra. Si ya habia adivinado 1 o mas letras, se le suman las letras adivinadas. En caso de que el usuaria ponga "a" y halla 3 a en la palabra. Esto daria 3
	sub r11,r11,#1 // Resto 1 a mi contador de @ arrobas ya que adivino 1 letra
	ldr r12,=palabraMisteriosa1
	add r12,r8 // igualo el puntero a la pocicion de la letra adivinada
	strb r1,[r12] // Reemplazo el arroba de palabraMisteriosa. El arroba reemplazado hace referencia a la mismo pocicion en la que se encontro la letra. Mostrando asi la letra que adivinaste
	b AcaLLameAVerificacionCorrecta // vuelvo al lugar de donde la funcion fue llamada

punteria:
//-------- Imprimo que ingrese X --------------
	ldr r1,=ingresarX
	mov r2,#29
	bl imprimirString
//-------- Pido Letra al usuario. La paso a Numero y la comparo con la coordenada X --------
	bl pedirNumeroX // devuelve la direccion en donde se guardo la letra pedida
	ldrb r10,[r1] // tomo el primer byte que me dio el usuario
	cmp r10,#'2'
	beq verificoSegundo // Salto a suamr a r5 #1 para verificar que su primer byte fue 2
	b perdiste // si no es igual, perdio
verificoSegundo:
	add r1,#1 // paso el puntero al byte numero 2 del usuario
	ldrb r10,[r1] // tomo el valor de ese byte
	cmp r10,#'3' // Comparo el segundo digito del input del usuario con 3
	beq ingreseY1 // salto por comparacion a que ingrese las coordenadas de Y
	b perdiste

ingreseY1:
//-------- Imprimo que ingrese Y --------------
	ldr r1,=ingresarY
	mov r2,#29
	bl imprimirString
//-------- Pido Letra al usuario. La paso a Numero y la comparo con la coordenada X --------
	bl pedirNumeroY // devuelve la direccion en donde se guardo la letra pedida
	ldrb r1,[r1] // tomo el contenido del input  que me dio el usuario
	mov r7,#'7' // 7  es El numero correcto de la coordenada en Y
	cmp r1,r7 // comparo el input del usuario en ascii con la constante 23 en ascii
	beq ganaste
	b perdiste


restarVida:
	sub r5,r5,#1
	cmp r5,#6
	beq dibujar_Cabeza
	cmp r5,#5
	beq dibujar_Torso
	cmp r5,#4
	beq dibujar_BrazoIzq
	cmp r5,#3
	beq dibujar_BrazoDer
	cmp r5,#2
	beq dibujar_Cadera
	cmp r5,#1
	beq dibujar_PieIzq
	cmp r5,#0
	beq dibujar_PieDer
	b cicloParaPedirLetra

ganaste:
	//------------ Imprimo el mensaje "Ganaste el juego.¡Felicidades! . y  doy salida al sistema para cerrar el juego
	ldr r1,=mensajeGanaste  // puntero a la direccion de memoria del mensaje a imprimir
	mov r2,#32  //longitud del mensaje a imprimir
	bl imprimirString // llamo a la funcion para imprimir el mensaje con los parametros anteriores
	mov r7,#1 // parametro para salida al sistema
	swi 0 // llamo a la interrupcion para ejecutar la salida al sistema
perdiste:
	//------- Imprimo el mensaje "Te quedaste sin vidas. Perdiste el juego. Y doy salida al sistema para cerrar el juego"
	ldr r1,=mensajePerdiste // Puntero a direccion de memoria del mensaje a imprimir
	mov r2,#42 // Longitud del mensaje a imprimir
	bl imprimirString // Llamo a la funcion para imprimir el mensaje con los parametros anteriores
	mov r7,#1  // parametro para Salida al sistema
	swi 0 // llamo a la interrupcion para ejecutar la salida al sistema

dibujar_Cabeza:
	push {r1}
	push {r0}
	ldr r0,=fila8 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=cabeza // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#23 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_Torso:
	push {r1}
	push {r0}
	ldr r0,=fila9 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=torso // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#23 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_BrazoIzq:
	push {r1}
	push {r0}
	ldr r0,=fila9 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=brazoIzq // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#22 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	ldr r1,=mapa // dejo a mapa en su direccion original para evitar errores futuros
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_BrazoDer:
	push {r1}
	push {r0}
	ldr r0,=fila9 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=brazoDer // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#24 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	ldr r1,=mapa // dejo a mapa en su direccion original para evitar errores futuros
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_Cadera:
	push {r1}
	push {r0}
	ldr r0,=fila10 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=cadera // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#23 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	ldr r1,=mapa // dejo a mapa en su direccion original para evitar errores futuros
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_PieIzq:
	push {r1}
	push {r0}
	ldr r0,=fila11 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=pieIzq // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#22 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	ldr r1,=mapa // dejo a mapa en su direccion original para evitar errores futuros
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

dibujar_PieDer:
	push {r1}
	push {r0}
	ldr r0,=fila11 // Direccion a la memoria de fila8
	ldr r0,[r0] // Tomo el valor necesario para ir a la fila8
	ldr r1,=mapa
	add r1,r1,r0 // la direccion de r1 (mapa) ahora hace referencia a la fila 8
	ldr r3,=pieDer // direccion de dibujo de la parte del cuerpo
	ldrb r3,[r3] // Caracter ascii representando la parte del cuerpo
	mov r2,#24 // Constante con referencia a la columna
	strb r3,[r1,r2] // Guardo en la direccion de memoria [fila,columna] del mapa. La parte correspondiente a dibujar
	ldr r1,=mapa // dejo a mapa en su direccion original para evitar errores futuros
	pop {r0}
	pop {r1}
	b cicloParaPedirLetra

